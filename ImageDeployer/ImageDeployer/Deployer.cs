using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BucketClient;
using GlobExpressions;
using Kirinnee.Helper;
using Kirinnee.ImageOrchestrator;
using Kirinnee.ImageOrchestrator.Transformers;

namespace Kirinnee.ImageDeployer
{
    public class Deployer
    {
        public const string HashFile = "kirinnee_image_deployer_hash.txt";
        private readonly BucketCredentialResolver _bucketClientFactory;
        private readonly HashResolver _hashResolver;
        private readonly Logger console;


        public Deployer(Logger logger, BucketCredentialResolver bucketClientFactory, HashResolver hashResolver)
        {
            console = logger;
            _bucketClientFactory = bucketClientFactory;
            _hashResolver = hashResolver;
        }

        public async Task Deploy(Configuration config, Dictionary<string, string> variables)
        {
            console.Log("Reading Files...");

            // Load raw files as virtual images
            var rawFiles = Glob.Files(config.Input, "**/*.*")
                .Select(e => new VirtualImage(config.Input, config.Target, e));

            // Load image bytes into RAM
            var files = rawFiles
                .Select(s => s.Read());
            console.Log("Files Loaded in memory");

            //Attempt to find client
            console.Log("Look for bucket credentials...");
            var bucketClient = await _bucketClientFactory.GetClient(config, variables);
            if (bucketClient == null) console.Log("Not deploying to bucket!");

            //With possible client info and config, try sort out images, and hashes
            console.Log("Attempt to compute hash...");
            var (blobs, hashes) = await _hashResolver.ResolveHash(files, config, bucketClient);
            console.Log("Hash Computed!");


            console.Log("Sorting into resizable...");
            var imageMime = new[] {"image/png", "image/jpeg"};
            var image = new List<VirtualImage>();
            var nonImage = new List<VirtualImage>();
            foreach (var blob in blobs)
                if (imageMime.Contains(blob.MimeType)) image.Add(blob);
                else nonImage.Add(blob);

            console.Log("Sorted!");

            //Sort out resizing
            console.Log("Resizing...");
            var resizedImages = config.Sizes.AsParallel().Select(v =>
            {
                var resize = new PercentageResizeTransformer(v.Value / 100, imageMime);
                return image.Select(e => e.Clone().Transform(resize)).Select(e => e.Rename(f => f + "_" + v.Key));
            }).SelectMany(e => e).ToArray();
            console.Log("Resize complete!");

            //Create compression transformers
            console.Log("Compressing via byte transformers...");
            var transformers = new List<ImageTransformer>();
            if (config.UseJpegOptim) transformers.Add(new JpegOptimTransformer(config.JpegOptim));
            if (config.UsePngQuant) transformers.Add(new PngQuantTransformer(config.PngQuant));
            if (config.UseWuQuant) transformers.Add(new WuQuantTransformer());
            var images = resizedImages.Select(b => b.TransformSet(transformers, console)).ToArray();
            console.Log("Compressed!");


            //Combine blobs
            var allBlobs = images.Concat(nonImage).ToArray();


            if (config.Bucket == null)
            {
                console.Log("Begin writing to file...");
                foreach (var b in allBlobs)
                {
                    b.Write();
                    Console.ForegroundColor = ConsoleColor.Green;
                    console.Log("Written: " + b.RawFile);
                    Console.ResetColor();
                }

                console.Log("Finished Writing to disk!");
            }
            else
            {
                console.Log("Setting up bucket...");
                var exist = await bucketClient.ExistBucket(config.Target);
                if (!exist)
                {
                    var createResult = await bucketClient.CreateBucket(config.Target);
                    if (!createResult.Success)
                        throw new InvalidOperationException("Failed creating non-existing bucket: \n" +
                                                            "Status Code:\n" + createResult.StatusCode + "\n" +
                                                            createResult.Message);
                }

                var bucket = await bucketClient.GetBucket(config.Target);
                console.Log("Obtained bucket data!");
                if (config.Bucket.Public)
                {
                    var policyResult = await bucketClient.SetReadPolicy(config.Target, ReadAccess.Public);
                    if (!policyResult.Success)
                        throw new InvalidOperationException("Failed making bucket public: \n" +
                                                            "Status Code:\n" + policyResult.StatusCode + "\n" +
                                                            policyResult.Message);
                }

                if (config.Bucket.GetCors != null && config.Bucket.GetCors.Length > 0)
                {
                    var corsResult = await bucketClient.SetGETCors(config.Target, config.Bucket.GetCors);
                    if (!corsResult.Success)
                        throw new InvalidOperationException("Failed making bucket public: \n" +
                                                            "Status Code:\n" + corsResult.StatusCode + "\n" +
                                                            corsResult.Message);
                }

                console.Log("Write new hash...");
                if (config.Bucket.CheckHash)
                {
                    var checksum = hashes.Select(e => e.ToString()).JoinBy("\n").ToUtf8();
                    var checksumResult = await bucket.PutBlob(checksum, HashFile);
                    if (!checksumResult.Success)
                        throw new InvalidOperationException("Failed putting checksum into bucket: \n" +
                                                            "Status Code:\n" + checksumResult.StatusCode + "\n" +
                                                            checksumResult.Message);
                }

                foreach (var b in allBlobs)
                {
                    if (Environment.GetEnvironmentVariable("BUCKET_CLIENT_DEBUG") == "VERBOSE")
                    {
                        Console.WriteLine("Raw: " + b.RawFile);
                        Console.WriteLine("Default MimeType: " + b.MimeType);
                        Console.WriteLine("KirinMime: " + b.Content.KirinMime());
                    }

                    var result = await bucket.PutBlob(b.Content, Path.Combine(config.Bucket.Prepend, b.RawFile));
                    if (!result.Success)
                        throw new InvalidOperationException($"Failed putting blob into bucket: {b.RawFile}\n" +
                                                            "Status Code:\n" + result.StatusCode + "\n" +
                                                            result.Message);
                    Console.ForegroundColor = ConsoleColor.Green;
                    console.Log("Deployed: " + b.RawFile);
                    Console.ResetColor();
                }

                console.Log("Completed!");
            }


            if (config.Delete)
            {
                console.Log("Deleting source images...");
                foreach (var f in rawFiles)
                {
                    try
                    {
                        File.Delete(f.FullFrom);
                    }
                    catch (Exception e)
                    {
                        console.Log($"Failed to delete file at {f.FullFrom}: ${e.Message}");
                    }
                }

                console.Log("Deleted!");
            }
        }
    }
}