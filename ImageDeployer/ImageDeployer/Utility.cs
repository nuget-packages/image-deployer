using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using Kirinnee.ImageOrchestrator;
using Kirinnee.ImageOrchestrator.Transformers;
using Newtonsoft.Json;

namespace Kirinnee.ImageDeployer
{
    public static class Utility
    {
        public static Dictionary<string, double> Dyn2Dict(dynamic dynObj)
        {
            var s = JsonConvert.SerializeObject(dynObj);
            return JsonConvert.DeserializeObject<Dictionary<string, double>>(s);
        }

        public static string[] Dyn2Arr(dynamic dynObj)
        {
            if (dynObj == null) return new string[] { };
            var s = JsonConvert.SerializeObject(dynObj);
            return JsonConvert.DeserializeObject<string[]>(s);
        }

        public static string ToUtf8(this byte[] arr)
        {
            return Encoding.UTF8.GetString(arr);
        }

        public static byte[] ToUtf8(this string x)
        {
            return Encoding.UTF8.GetBytes(x);
        }

        public static bool Matches(this string x, params string[] list)
        {
            return list.Any(e => e == x);
        }


        public static VirtualImage TransformSet(this VirtualImage x, IEnumerable<ImageTransformer> transformers,
            Logger l)
        {
            l.Log("Starting to Compress: " + x.RawFile + $" of {x.Content.Length} bytes");
            if (x.Content.Length < 4121) return x;
            var r = transformers.Aggregate(x, (current, t) => current.Transform(t));
            l.Log("Compressed: " + x.RawFile);
            return r;
        }

        public static T[] Concat<T>(this T[] a1, T[] a2)
        {
            var arrayCombined = new T[a1.Length + a2.Length];
            Array.Copy(a1, 0, arrayCombined, 0, a1.Length);
            Array.Copy(a2, 0, arrayCombined, a1.Length, a2.Length);
            return arrayCombined;
        }
    }
}