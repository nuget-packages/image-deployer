using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Kirinnee.ConsoleHelp;
using Kirinnee.Helper;
using Kirinnee.ImageOrchestrator;
using Newtonsoft.Json;

namespace Kirinnee.ImageDeployer
{
    public class DeployCommand : ICommandObject
    {
        public async Task<CommandResult> Execute(string[] fullCommand, string[] rawArguments,
            Dictionary<string, string> variables, string[] flags)
        {
            //Object initialization
            var configFinder = new ConfigurationFinder();
            var jsonBridge = new JsonToConfigBridge();
            var credResolver = new BucketCredentialResolver();
            var hashResolver = new HashResolver();

            try
            {
                var quiet = flags.Contains("quiet");
                var console = new Logger(quiet);
                var deployer = new Deployer(console, credResolver, hashResolver);
                var configPath = variables.FirstOrDefault(s => s.Key == "config").Value;
                var configFile = await configFinder.GetConfig(configPath);
                var jsonList = JsonConvert.DeserializeObject<List<dynamic>>(configFile);

                var config = jsonList.Select(jsonBridge.JsonToConfig)
                    .Select(c => jsonBridge.UpdateWithVariables(c, variables))
                    .ToArray();
                var tasks = config.Select(s => deployer.Deploy(s, variables));
                await Task.WhenAll(tasks);
                return new CommandResult(true, "Deployed!");
            }
            catch (Exception e)
            {
                return new CommandResult(false, e.Message + "\n" + e.StackTrace);
            }
        }

        public string Name { get; } = "Deploy";
        public string Description { get; } = "Deploys everything in config file";

        public IEnumerable<string> PossibleFlags { get; } = new[]
        {
            "quiet"
        };

        public IEnumerable<string> PossibleVariables { get; } = new[]
        {
            "config",
            "secret",

            "accessId",
            "accessSecret",
            "region",

            "accountName",
            "accessToken",

            "projectID",
            "jsonSecret",

            "target",
            "input"
        };

        public string[] Command { get; } = {"deploy"};
        public string PerfectCommand => Command.JoinBy(" ");
        public string Usage { get; } = "deploy <option>";
    }
}