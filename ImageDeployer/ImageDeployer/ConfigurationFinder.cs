using System.IO;
using System.Threading.Tasks;

namespace Kirinnee.ImageDeployer
{
    public class ConfigurationFinder
    {
        private const string DefFile = "image.deploy.config.json";
        public async Task<string> GetConfig(string path = null)
        {
            if (path == null) path = DefFile;
            return await File.ReadAllTextAsync(path);
        }
    }
}