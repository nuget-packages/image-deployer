using System.Collections.Generic;
using Minimage;

namespace Kirinnee.ImageDeployer
{
    public class Configuration
    {
        public string Key;
        public double Width;
        public IDictionary<string, double> Sizes;

        //Use bucket configuration if want bucket!
        public BucketConfiguration Bucket;

        public string Target;
        public string Input;

        public JpegOptimOptions JpegOptim = null;
        public PngQuantOptions PngQuant = null;

        public bool Delete = false;
        public bool UseJpegOptim = false;
        public bool UsePngQuant = false;
        public bool UseWuQuant = false;
    }

    public class BucketConfiguration
    {
        public string Provider;
        public bool Public = false;
        public string[] GetCors = { };
        public string Prepend = ".";
        public bool CheckHash = false;
    }
}