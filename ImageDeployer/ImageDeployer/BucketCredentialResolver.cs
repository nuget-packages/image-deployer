using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using BucketClient;
using BucketClient.Library.Credentials;
using Google.Apis.Auth.OAuth2;
using Kirinnee.ConsoleHelp;
using Newtonsoft.Json;
using ICredential = BucketClient.ICredential;

namespace Kirinnee.ImageDeployer
{
    public class BucketCredentialResolver
    {
        public async Task<IBucketClient> GetClient(Configuration config, Dictionary<string, string> variables)
        {
            if (config.Bucket == null) return null;
            var t = config.Bucket.Provider.ToLower();
            if (t.Matches("aws", "amazonwebservice", "amazon"))
            {
                var awsCred = await ResolveAwsCred( variables);
                return BucketClientFactory.CreateClient(CloudServiceProvider.AWS, awsCred);
            }

            if (t.Matches("do", "digitalocean"))
            {
                var doCred = await ResolveDoCred( variables);
                return BucketClientFactory.CreateClient(CloudServiceProvider.DigitalOcean, doCred);
            }

            if (t.Matches("microsoft", "ms", "azure"))
            {
                var azureCred = await ResolveAzureCred( variables);
                return BucketClientFactory.CreateClient(CloudServiceProvider.Azure, azureCred);
            }

            if (t.Matches("google", "gcp", "googlecloudplatform"))
            {
                var gcpCred = await ResolveGcpCred( variables);
                return BucketClientFactory.CreateClient(CloudServiceProvider.GCP, gcpCred);
            }

            return null;
        }

        private async Task<ICredential> ResolveGcpCred( Dictionary<string, string> variables)
        {
            //First try to read from secret, if it exist
            var fileSecrets = await ObtainSecretFromFile(variables);
            string projectId = fileSecrets?.projectID ?? "";
            string jsonSecret = fileSecrets?.jsonSecret != null ? JsonConvert.SerializeObject(fileSecrets.jsonSecret) : "";
            //Now try to read from stdin, and overwrite any new data
            var stdinSecrets = await ObtainSecretFromStream();
            projectId = stdinSecrets?.projectID ?? projectId;
            jsonSecret = stdinSecrets?.jsonSecret != null
                ? JsonConvert.SerializeObject(stdinSecrets.jsonSecret)
                : jsonSecret;
            //now replace with variables
            if (variables.ContainsKey("projectID")) projectId = variables["projectID"];
            if (variables.ContainsKey("jsonSecret")) jsonSecret = variables["jsonSecret"];
            //Mark error if credentials are not there!
            if (projectId == "")
                throw new ArgumentException("No project ID found! Cannot connect to Google Cloud Platform");
            if (jsonSecret == "")
                throw new ArgumentException("No json Secret found! Cannot connect to Google Cloud Platform");
            return new GCPCredential(projectId, jsonSecret);
        }

        private async Task<ICredential> ResolveAzureCred(Dictionary<string, string> variables)
        {
            //First try to read from secret, if it exist
            var fileSecrets = await ObtainSecretFromFile(variables);
            string accountName = fileSecrets?.accountName ?? "";
            string accessToken = fileSecrets?.accessToken ?? "";
            //Now try to read from stdin, and overwrite any new data
            var stdinSecrets = await ObtainSecretFromStream();
            accountName = stdinSecrets?.accountName ?? accountName;
            accessToken = stdinSecrets?.accessToken ?? accessToken;
            //now replace with variables
            if (variables.ContainsKey("accountName")) accountName = variables["accountName"];
            if (variables.ContainsKey("accessToken")) accessToken = variables["accessToken"];
            //Mark error if credentials are not there!
            if (accountName == "")
                throw new ArgumentException("No account name found! Cannot connect to Azure Blob Storage");
            if (accessToken == "")
                throw new ArgumentException("No access token found! Cannot connect to Azure Blob Storage");
            return new AzureCredential(accountName, accessToken);
        }

        private async Task<ICredential> ResolveAwsCred(Dictionary<string, string> variables)
        {
            //First try to read from secret, if it exist
            var fileSecrets = await ObtainSecretFromFile(variables);
            string accessId = fileSecrets?.accessId ?? "";
            string accessSecret = fileSecrets?.accessSecret ?? "";
            string region = fileSecrets?.region ?? "";
            //Now try to read from stdin, and overwrite any new data
            var stdinSecrets = await ObtainSecretFromStream();
            accessId = stdinSecrets?.accessId ?? accessId;
            accessSecret = stdinSecrets?.accessSecret ?? accessSecret;
            region = stdinSecrets?.region ?? region;
            //now replace with variables
            if (variables.ContainsKey("accessId")) accessId = variables["accessId"];
            if (variables.ContainsKey("accessSecret")) accessSecret = variables["accessSecret"];
            if (variables.ContainsKey("region")) region = variables["region"];
            //Mark error if credentials are not there!
            if (accessId == "") throw new ArgumentException("No access ID found! Cannot connect to AWS");
            if (accessSecret == "") throw new ArgumentException("No access Secret found! Cannot connect to AWS");
            if (region == "") throw new ArgumentException("No region found! Cannot connect to AWS");

            return new AWSCredential(accessId, accessSecret, region);
        }

        private async Task<ICredential> ResolveDoCred( Dictionary<string, string> variables)
        {
            //First try to read from secret, if it exist
            var fileSecrets = await ObtainSecretFromFile(variables);
            string accessId = fileSecrets?.accessId ?? "";
            string accessSecret = fileSecrets?.accessSecret ?? "";
            string region = fileSecrets?.region ?? "";
            //Now try to read from stdin, and overwrite any new data
            var stdinSecrets = await ObtainSecretFromStream();
            accessId = stdinSecrets?.accessId ?? accessId;
            accessSecret = stdinSecrets?.accessSecret ?? accessSecret;
            region = stdinSecrets?.region ?? region;
            //now replace with variables
            if (variables.ContainsKey("accessId")) accessId = variables["accessId"];
            if (variables.ContainsKey("accessSecret")) accessSecret = variables["accessSecret"];
            if (variables.ContainsKey("region")) region = variables["region"];
            //Mark error if credentials are not there!
            if (accessId == "") throw new ArgumentException("No access ID found! Cannot connect to Digital Ocean");
            if (accessSecret == "")
                throw new ArgumentException("No access Secret found! Cannot connect to Digital Ocean");
            if (region == "") throw new ArgumentException("No region found! Cannot connect to Digital Ocean");

            return new DigitalOceanCredential(accessId, accessSecret, region);
        }

        private async Task<dynamic> ObtainSecretFromStream()
        {
            if (!Console.IsInputRedirected) return null;
            var stdin = await ConsoleHelper.ReadStdin();
            var configData = Encoding.UTF8.GetString(stdin);
            return JsonConvert.DeserializeObject<dynamic>(configData);
        }

        private async Task<dynamic> ObtainSecretFromFile(Dictionary<string, string> variables)
        {
            if (variables == null) throw new ArgumentNullException(nameof(variables));
            //Try obtain Secret first
            var secret = "./secret.config.json";
            if (variables.ContainsKey("secret")) secret = variables["secret"];
            var path = Path.Combine(secret);
            if (!File.Exists(path)) return null;
            var json = await File.ReadAllTextAsync(path);
            return JsonConvert.DeserializeObject<dynamic>(json);
        }
    }
}