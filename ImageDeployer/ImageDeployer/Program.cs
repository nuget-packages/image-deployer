﻿using System.Threading.Tasks;
using Kirinnee.ConsoleHelp;

namespace Kirinnee.ImageDeployer
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var exe = new CommandExecutor("Image Deploy",
                "Image Deploy is a Command Line Interface that helps with deploying image to bucket. " +
                "It provides compression methods for png and jpeg, and also allows for resizing base on " +
                "rImage configuration.", "2.1.0", new ICommandObject[] {new DeployCommand()});
            Async(args, exe).GetAwaiter().GetResult();
        }

        public static async Task Async(string[] args, ICommandExecutor exe)
        {
            await exe.ExecuteCommand(args);
        }
    }
}