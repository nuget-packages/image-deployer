using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using BucketClient;
using Google.Apis.Storage.v1;
using Kirinnee.Helper;
using Kirinnee.ImageOrchestrator;
using Newtonsoft.Json;

namespace Kirinnee.ImageDeployer
{
    public class HashObject
    {
        public string Address;
        public string MD5;
        public string CRC32;

        public override bool Equals(object obj)
        {
            if (obj is HashObject other)
            {
                return Address == other.Address && MD5 == other.MD5 && CRC32 == other.CRC32;
            }

            return false;
        }

        public static HashObject FromString(string i)
        {
            var x = i.SplitBy(" ~ ").ToArray();
            return new HashObject()
            {
                Address = x[0],
                MD5 = x[1],
                CRC32 = x[2]
            };
        }

        public override string ToString()
        {
            return Address + " ~ " + MD5 + " ~ " + CRC32;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public static class HashExtension
    {
        public static HashObject Hash(this VirtualImage image, Configuration config)
        {
            var unique = (config.Key + config.Width + JsonConvert.SerializeObject(config.Sizes) +
                          JsonConvert.SerializeObject(config.UseJpegOptim) +
                          JsonConvert.SerializeObject(config.UsePngQuant) +
                          JsonConvert.SerializeObject(config.UseWuQuant) +
                          JsonConvert.SerializeObject(config.PngQuant) +
                          JsonConvert.SerializeObject(config.JpegOptim)).ToUtf8();
            var bytes = image.Content.Concat(unique);
            var MD5 = bytes.MD5Hash();
            var CRC32 = bytes.CRC32Hash();
            return new HashObject()
            {
                Address = image.RawFile,
                MD5 = MD5,
                CRC32 = CRC32
            };
        }

        private static string CRC32Hash(this byte[] array)
        {
            using (var crc32 = new Crc32())
            {
                var hash = crc32.ComputeHash(array);
                return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
            }
        }

        private static string MD5Hash(this byte[] array)
        {
            using (var md5 = MD5.Create())
            {
                var hash = md5.ComputeHash(array);
                return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
            }
        }
    }

    public class HashResolver
    {
        public async Task<(IEnumerable<VirtualImage>, IEnumerable<HashObject>)> ResolveHash(
            IEnumerable<VirtualImage> input, Configuration config,
            IBucketClient client)
        {
            if (client == null || !config.Bucket.CheckHash) return (input, null);
            var changedHashes = new List<HashObject>();
            var neededImages = new List<VirtualImage>();
            var hashes = await LoadHashFromServer(client, config.Target);
            foreach (var image in input)
            {
                var addHash = false;
                if (hashes.ContainsKey(image.RawFile))
                {
                    if (!hashes[image.RawFile].Equals(image.Hash(config)))
                        addHash = true;
                }
                else
                {
                    addHash = true;
                }

                var h = image.Hash(config);
                changedHashes.Add(h);
                if (addHash) neededImages.Add(image);
            }

            return (neededImages, changedHashes);
        }

        private async Task<Dictionary<string, HashObject>> LoadHashFromServer(IBucketClient client, string target)
        {
            var existBucket = await client.ExistBucket(target);
            if (!existBucket) return new Dictionary<string, HashObject>();
            var bucket = await client.GetBucket(target);
            const string fileName = Deployer.HashFile;
            var exist = await bucket.ExistBlob(fileName);
            if (!exist) return new Dictionary<string, HashObject>();

            var data = await bucket.GetBlob(fileName);
            var s = data.ToUtf8().SplitBy("\n");
            return s.Select(HashObject.FromString).ToDictionary(x => x.Address, x => x);
        }

    }
}