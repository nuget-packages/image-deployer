using System;
using Newtonsoft.Json;

namespace Kirinnee.ImageDeployer
{
    public class Logger
    {
        private readonly bool _quiet;

        public Logger(bool quiet)
        {
            _quiet = quiet;
        }

        public void Log(object any)
        {
            if (_quiet) return;
            if (any is string || any is int || any is long || any is byte ||
                any is float || any is double || any is uint || any is ulong
                || any is sbyte)
            {
                Console.WriteLine(any);
            }
            else
            {
                Console.WriteLine(JsonConvert.SerializeObject(any));
            }
        }
    }
}