using System.Collections.Generic;
using Minimage;

namespace Kirinnee.ImageDeployer
{
    public class JsonToConfigBridge
    {
        public Configuration JsonToConfig(dynamic json)
        {
            var config = new Configuration
            {
                Key = json.key ?? "def",
                Width = json.width ?? 2000,
                Sizes = json.sizes != null
                    ? Utility.Dyn2Dict(json.sizes)
                    : new Dictionary<string, double>() {{"def", 100}},
                Target = json.target ?? "",
                Input = json.input ?? "",
                Delete = json.delete ?? false,
            };
            if (json.bucket == null) config.Bucket = null;
            else
            {
                var bConfig = json.bucket;

                config.Bucket = new BucketConfiguration
                {
                    Prepend = bConfig.prepend ?? ".",
                    Provider = bConfig.provider ?? "",
                    Public = bConfig["public"] ?? false,
                    GetCors = Utility.Dyn2Arr(bConfig["get-cors"]) ?? new string[] { },
                    CheckHash = bConfig["check-hash"] ?? false
                };
            }

            if (json.compressions == null) return config;
            config.UseJpegOptim = json.compressions.jpegOptim != null;
            config.UsePngQuant = json.compressions.pngQuant != null;
            config.UseWuQuant = json.compressions.wuQuant ?? false;
            if (config.UseJpegOptim)
            {
                var jpeg = json.compressions.jpegOptim;
                var options = new JpegOptimOptions()
                {
                    Overwrite = true,
                    Force = false
                };
                options.StripAll = jpeg.strip ?? options.StripAll;
                options.Progressive = jpeg.progressive ?? options.Progressive;
                options.Quality = jpeg.quality ?? options.Quality;
                config.JpegOptim = options;
            }

            if (config.UsePngQuant)
            {
                var png = json.compressions.pngQuant;
                var options = new PngQuantOptions();
                options.Bit = png.bit ?? options.Bit;
                options.Speed = png.speed ?? options.Speed;
                options.QualityMinMax = (png["min-quality"] ?? 65, png["max-quality"] ?? 80);
                config.PngQuant = options;
            }

            return config;
        }

        public Configuration UpdateWithVariables(Configuration input, Dictionary<string, string> variables)
        {
            if (variables.ContainsKey("target")) input.Target = variables["target"];
            if (variables.ContainsKey("input")) input.Input = variables["input"];

            return input;
        }
    }
}