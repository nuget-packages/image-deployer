﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Kirinnee.ConsoleHelp;

namespace TestConsole
{
    class Program
    {
        private static void Main(string[] args)
        {
            var exe = new CommandExecutor("TestConsole", "Test console to test integration with Bucket Client", "0.0.1",
                new List<ICommandObject>() {new CreateCommand(), new DeleteCommand()});
            AsyncMain(args, exe).GetAwaiter().GetResult();
        }

        static async Task AsyncMain(string[] args, ICommandExecutor exe)
        {
            await exe.ExecuteCommand(args);
        }
    }
}