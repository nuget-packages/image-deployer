using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BucketClient;
using Kirinnee.ConsoleHelp;
using Kirinnee.Helper;
using Kirinnee.ImageDeployer;
using Newtonsoft.Json;

namespace TestConsole
{
    public class DeleteCommand : ICommandObject
    {
        public async Task<CommandResult> Execute(string[] fullCommand, string[] rawArguments,
            Dictionary<string, string> variables, string[] flags)
        {
            //Object initialization
            var configFinder = new ConfigurationFinder();
            var jsonBridge = new JsonToConfigBridge();
            var credentialResolver = new BucketCredentialResolver();
            try
            {
                var configPath = variables.FirstOrDefault(s => s.Key == "config").Value;
                var configFile = await configFinder.GetConfig(configPath);
                var dynConfig = JsonConvert.DeserializeObject<dynamic>(configFile);

                Configuration config = jsonBridge.JsonToConfig(dynConfig);
                config = jsonBridge.UpdateWithVariables(config, variables);
                var client = await credentialResolver.GetClient(config, variables);

                var exist = await client.ExistBucket("deploy");
                if (!exist) return new CommandResult(true, "Deleted");
                var bucket = await client.GetBucket("deploy");
                var result = await bucket.DeleteBlob("sop.png");
                Console.WriteLine(result.ToString());
                return new CommandResult(true, "Deleted!");
            }
            catch (Exception e)
            {
                return new CommandResult(false, e.Message + " \n" + e.StackTrace);
            }
        }

        public string Name { get; } = "Delete";
        public string Description { get; } = "Delete sop on bucket";

        public IEnumerable<string> PossibleFlags { get; } = new string[]
        {
            "check-hash"
        };

        public IEnumerable<string> PossibleVariables { get; } = new[]
        {
            "config",
            "secret",

            "accessId",
            "accessSecret",
            "region",
            "accountName",
            "accessToken",
            "projectID",
            "jsonSecret",

            "target",
            "input",
            "bucket"
        };

        public string[] Command { get; } = {"del"};
        public string PerfectCommand => Command.JoinBy(" ");
        public string Usage { get; } = "del <options>";
    }

    public class CreateCommand : ICommandObject
    {
        public async Task<CommandResult> Execute(string[] fullCommand, string[] rawArguments,
            Dictionary<string, string> variables, string[] flags)
        {
            //Object initialization
            var configFinder = new ConfigurationFinder();
            var jsonBridge = new JsonToConfigBridge();
            var credentialResolver = new BucketCredentialResolver();
            try
            {
                var configPath = variables.FirstOrDefault(s => s.Key == "config").Value;
                var configFile = await configFinder.GetConfig(configPath);
                var dynConfig = JsonConvert.DeserializeObject<dynamic>(configFile);

                Configuration config = jsonBridge.JsonToConfig(dynConfig);
                config = jsonBridge.UpdateWithVariables(config, variables);
                var client = await credentialResolver.GetClient(config, variables);
                if (client == null) return new CommandResult(false, "Cannot create client!");
                var exist = await client.ExistBucket("deploy");
                if (!exist)
                {
                    var r = await client.CreateBucket("deploy");
                    Console.WriteLine(r.Message + " " + r.Success + " " + r.StatusCode);
                }

                var bucket = await client.GetBucket("deploy");
                var image = await File.ReadAllBytesAsync(Path.Combine("./sop.png"));
                var result = await bucket.PutBlob(image, "sop.png");
                Console.WriteLine(result.Message + " " + result.Success + " " + result.StatusCode);
                return new CommandResult(true, "Created!");
            }
            catch (Exception e)
            {
                return new CommandResult(false, e.Message + " \n" + e.StackTrace);
            }
        }

        public string Name { get; } = "Create";
        public string Description { get; } = "Create sop on bucket";

        public IEnumerable<string> PossibleFlags { get; } = new string[]
        {
            "check-hash"
        };

        public IEnumerable<string> PossibleVariables { get; } = new[]
        {
            "config",
            "secret",

            "accessId",
            "accessSecret",
            "region",
            "accountName",
            "accessToken",
            "projectID",
            "jsonSecret",

            "target",
            "input",
            "bucket"
        };

        public string[] Command { get; } = {"create"};
        public string PerfectCommand => Command.JoinBy(" ");
        public string Usage { get; } = "create <options>";
    }
}