using System.Collections.Generic;
using Kirinnee.ImageDeployer;
using Minimage;
using Newtonsoft.Json;
using Xunit;

namespace test
{
    public class JsonToConfigBridgeTest
    {
        private readonly JsonToConfigBridge _bridge = new JsonToConfigBridge();

        [Fact]
        public void Should_set_def_key_from_def()
        {
            const string test = @"
{
    ""key"": ""mainkey"",
    ""target"": ""aws"",
    ""input"": ""../relative/folder""
}
";
            var configFile = JsonConvert.DeserializeObject<dynamic>(test);
            Configuration config = _bridge.JsonToConfig(configFile);
            Assert.Equal("mainkey", config.Key);
        }

        [Fact]
        public void Should_set_to_default_value_if_they_are_not_set()
        {
            const string test = @"
{
    ""target"": ""aws"",
    ""input"": ""../relative/folder""
}
";
            var configFile = JsonConvert.DeserializeObject<dynamic>(test);
            Configuration config = _bridge.JsonToConfig(configFile);
            Assert.Equal("def", config.Key);
            Assert.Equal(2000, config.Width);
            Assert.Equal(new Dictionary<string, double>() {{"def", 100}}, config.Sizes);
            Assert.Null(config.Bucket);
            Assert.False(config.UseJpegOptim);
            Assert.False(config.UsePngQuant);
            Assert.False(config.UseWuQuant);
            Assert.Null(config.JpegOptim);
            Assert.Null(config.PngQuant);
        }

        [Fact]
        public void Should_set_to_def_value_if_not_set_in_bucket_config()
        {
            const string test = @"
{
    ""target"": ""aws"",
    ""input"": ""../relative/folder"",
    ""bucket"": {}
}
";
            var configFile = JsonConvert.DeserializeObject<dynamic>(test);
            Configuration config = _bridge.JsonToConfig(configFile);
            Assert.Equal(config.Bucket.Prepend, ".");
            Assert.False(config.Bucket.Public);
            Assert.Empty(config.Bucket.Provider);
            Assert.Equal(config.Bucket.GetCors, new string[] { });
            Assert.False(config.Bucket.CheckHash);
        }

        [Fact]
        public void Should_set_def_width_as_number_in_json()
        {
            const string test = @"
{
    ""width"": 170,
    ""target"": ""aws"",
    ""input"": ""../relative/folder""
}
";
            var configFile = JsonConvert.DeserializeObject<dynamic>(test);
            Configuration config = _bridge.JsonToConfig(configFile);
            Assert.Equal(170d, config.Width);
        }

        [Fact]
        public void Should_set_dictionary_sizes_from_json_key_pair_value_under_sizes_label()
        {
            const string test = @"
{
    ""sizes"": {
        ""large"": 100,
        ""mid"": 50,
        ""small"": 20
    },
    ""target"": ""aws"",
    ""input"": ""../relative/folder""
}
";
            var configFile = JsonConvert.DeserializeObject<dynamic>(test);
            Configuration config = _bridge.JsonToConfig(configFile);
            var expected = new Dictionary<string, double>()
            {
                {"large", 100},
                {"mid", 50},
                {"small", 20}
            };
            Assert.Equal(expected, config.Sizes);
        }

        [Fact]
        public void Should_set_check_hash_from_json()
        {
            const string test = @"
{
    ""bucket"": {
        ""check-hash"" : true    
    },
    ""target"": ""test"",
    ""input"": ""../relative/folder""
}
";
            var configFile = JsonConvert.DeserializeObject<dynamic>(test);
            Configuration config = _bridge.JsonToConfig(configFile);
            Assert.True(config.Bucket.CheckHash);
        }
        
        [Fact]
        public void Should_set_provider_from_json()
        {
            const string test = @"
{
    ""bucket"": {
        ""provider"" : ""aws""    
    },
    ""target"": ""test"",
    ""input"": ""../relative/folder""
}
";
            var configFile = JsonConvert.DeserializeObject<dynamic>(test);
            Configuration config = _bridge.JsonToConfig(configFile);
            Assert.Equal(config.Bucket.Provider, "aws");
        }
        
        [Fact]
        public void Should_set_public_from_json()
        {
            const string test = @"
{
    ""bucket"": {
        ""public"" : true   
    },
    ""target"": ""test"",
    ""input"": ""../relative/folder""
}
";
            var configFile = JsonConvert.DeserializeObject<dynamic>(test);
            Configuration config = _bridge.JsonToConfig(configFile);
            Assert.True(config.Bucket.Public);
        }
        
        [Fact]
        public void Should_set_get_cors_from_json()
        {
            const string test = @"
{
    ""bucket"": {
        ""get-cors"" : [""*"", ""yes""]   
    },
    ""target"": ""test"",
    ""input"": ""../relative/folder""
}
";
            var configFile = JsonConvert.DeserializeObject<dynamic>(test);
            Configuration config = _bridge.JsonToConfig(configFile);
            Assert.Equal(config.Bucket.GetCors, new[]{"*", "yes"});
        }

        [Fact]
        public void Should_set_prepend_from_json()
        {
            const string test = @"
{
    ""bucket"": {
        ""prepend"" : ""some-folder""
    },
    ""target"": ""test"",
    ""input"": ""../relative/folder""
}
";
            var configFile = JsonConvert.DeserializeObject<dynamic>(test);
            Configuration config = _bridge.JsonToConfig(configFile);
            Assert.Equal("some-folder", config.Bucket.Prepend);
        }
        
        [Fact]
        public void Should_set_target_relative_path_of_target_from_json()
        {
            const string test = @"
{
    ""target"": ""aws"",
    ""input"": ""../relative/folder""
}
";
            var configFile = JsonConvert.DeserializeObject<dynamic>(test);
            Configuration config = _bridge.JsonToConfig(configFile);
            Assert.Equal("aws", config.Target);
        }


        [Fact]
        public void Should_set_input_relative_path_of_input_from_json()
        {
            const string test = @"
{
    ""target"": ""aws"",
    ""input"": ""../relative/folder""
}
";
            var configFile = JsonConvert.DeserializeObject<dynamic>(test);
            Configuration config = _bridge.JsonToConfig(configFile);
            Assert.Equal("../relative/folder", config.Input);
        }

        [Fact]
        public void Should_set_usePngQuant_to_true_if_there_are_pngquant_settings()
        {
            const string test = @"
{
    ""target"": ""aws"",
    ""input"": ""../relative/folder"",
    ""compressions"":{
        ""pngQuant"":{}
    }
}
";
            var configFile = JsonConvert.DeserializeObject<dynamic>(test);
            Configuration config = _bridge.JsonToConfig(configFile);
            ;
            Assert.True(config.UsePngQuant);
            Assert.False(config.UseJpegOptim);
            Assert.False(config.UseWuQuant);
        }

        [Fact]
        public void Should_set_useJpegOptim_to_true_if_there_are_jpegoptim_settings()
        {
            const string test = @"
{
    ""target"": ""aws"",
    ""input"": ""../relative/folder"",
    ""compressions"":{
        ""jpegOptim"":{}
    }
}
";
            var configFile = JsonConvert.DeserializeObject<dynamic>(test);
            Configuration config = _bridge.JsonToConfig(configFile);
            Assert.False(config.UsePngQuant);
            Assert.True(config.UseJpegOptim);
            Assert.False(config.UseWuQuant);
        }

        [Fact]
        public void Should_set_useJpegOptim_to_true_if_wuqaunt_is_set_to_true()
        {
            const string test = @"
{
    ""target"": ""aws"",
    ""input"": ""../relative/folder"",
    ""compressions"":{
        ""wuQuant"":true
    }
}
";
            var configFile = JsonConvert.DeserializeObject<dynamic>(test);
            Configuration config = _bridge.JsonToConfig(configFile);
            Assert.False(config.UsePngQuant);
            Assert.False(config.UseJpegOptim);
            Assert.True(config.UseWuQuant);
        }

        [Fact]
        public void Should_set_jpegOptimOptions_with_key_pair_value()
        {
            const string test = @"
{
    ""target"": ""aws"",
    ""input"": ""../relative/folder"",
    ""compressions"":{
        ""jpegOptim"":{
            ""strip"": true,
            ""progressive"": true,
            ""quality"": 53
        }
    }
}
";
            var configFile = JsonConvert.DeserializeObject<dynamic>(test);
            Configuration config = _bridge.JsonToConfig(configFile);
            Assert.True(config.JpegOptim.StripAll);
            Assert.True(config.JpegOptim.Progressive);
            Assert.Equal(53, config.JpegOptim.Quality);
        }

        [Fact]
        public void Should_set_pngQuantOptions_with_key_pair_values()
        {
            const string test = @"
{
    ""target"": ""aws"",
    ""input"": ""../relative/folder"",
    ""compressions"":{
        ""jpegOptim"":{
            ""strip"": true,
            ""progressive"": true,
            ""quality"": 53
        },
        ""pngQuant"":{
            ""bit"": 128,
            ""speed"" : 8,
            ""min-quality"": 30,
            ""max-quality"": 70
        }
    }
}
";
            var configFile = JsonConvert.DeserializeObject<dynamic>(test);
            Configuration config = _bridge.JsonToConfig(configFile);
            Assert.Equal(128, config.PngQuant.Bit);
            Assert.Equal(8, config.PngQuant.Speed);
            Assert.Equal((30, 70), config.PngQuant.QualityMinMax);
        }
    }
}